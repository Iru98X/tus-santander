import urllib
import json
import urllib.request
import math

###########################################  FUNCIONES  ######################################

# Funcion que toma por parametro una URL y devuelve el JSON de esta,
# formateado a lista de python
def getResponse(url):
    
    operUrl = urllib.request.urlopen(url)
    
    if(operUrl.getcode() == 200):
        data = operUrl.read()
        jsonData = json.loads(data.decode('utf-8'))
    else:
        print("Error receiving data", operUrl.getcode())
    return jsonData


# Funcion que dados unos datos de buses de Santander leidos de un JSON,
# y dados el ID de la parada y el numero de linea, devuelve una lista con
# los minutos restantes para la llegada de lo
# limite de tiempo expresado
def filtrar(datos, idParada, linea):
        
    # Filtrar los buses de la parada y linea correspondientes
    busesAhora = [x for x in data if 
                                x['ayto:paradaId'] == idParada]
    
    # Crea una lista dependiendo si se especifica la linea/bus concreta como parametro o no
    if(linea==None):
        ret = []
        for x in busesAhora:
            ret.append([x['ayto:etiqLinea'],
                    math.floor(int(x['ayto:tiempo1'])/60),
                    math.floor(int(x['ayto:tiempo2'])/60)])
    else:
        ret = [busesAhora[0]['ayto:etiqLinea'],
                math.floor(int(busesAhora[0]['ayto:tiempo1'])/60),
                math.floor(int(busesAhora[0]['ayto:tiempo2'])/60)]
    
    return ret



#############################################  MAIN  ########################################

# Coger JSON de la API
url = "http://datos.santander.es/api/rest/datasets/control_flotas_estimaciones.json?items=1064"
data = getResponse(url)['resources']

print(filtrar(data, idParada='482', linea='7C1'))
