import urllib
import json
import urllib.request
import datetime
from datetime import date
import time
import math
import requests

# Funcion que toma por parametro una URL y devuelve el JSON de esta,
# formateado a lista de python
def getResponse(url):
    
    start_time2 = time.time()
    operUrl = urllib.request.urlopen(url)
    print("\nPARTE urlopen  = %s seconds" % (time.time() - start_time2))
    
    if(operUrl.getcode() == 200):
        
        start_time2 = time.time()
        data = operUrl.read()
        print("PARTE read     = %s seconds" % (time.time() - start_time2))
         
        start_time2 = time.time()
        jsonData = json.loads(data.decode('utf-8'))
        print("PARTE loads    = %s seconds" % (time.time() - start_time2))
    else:
        print("Error receiving data", operUrl.getcode())
    return jsonData

# Funcion que dados unos datos de buses de Santander leidos de un JSON,
# y dados el numero de linea y el limite expresado en horas, devuelve
# un lista con los minutos restantes para la salida de los buses en el
# limite de tiempo expresado
def filtrar(datos, bus, periodo, limite):
    
    # Calcular el dia de hoy y si es festivo
    festivo = False
    festivos = ["2020-01-01", "2020-01-06", "2020-04-09", "2020-04-10", "2020-04-13", "2020-05-01",
                "2020-07-28", "2020-08-15", "2020-09-15", "2020-10-12", "2020-12-08", "2020-12-25"]
    today = date.today()
    hoy = str(today)
    if hoy in festivos:
        festivo = True
    day_name = ['Lunes', 'Martes', 'Miercoles',
                'Jueves', 'Viernes', 'Sabado', 'Domingo']
    day = datetime.datetime.strptime(hoy, '%Y-%m-%d').weekday() # Dia actual en numero
    
    # Calcula el tiempo actual
    now = datetime.datetime.now()
    horas = int(now.strftime("%H"))*3600
    minutos = int(now.strftime("%M"))*60
    segundos = int(now.strftime("%S"))
    tiempoSegundos = horas + minutos + segundos
    
    # Calcular el identificador segun el dia de hoy
    ident = "L"                                     # Laborable
    if day_name[day] == "Sabado":                   # Sabado
        ident = "S"
    elif day_name[day] == "Domingo" or festivo:     # Festivo
        ident = "F"
            
    # Filtrar los buses de ahora
    busesAhora = [x for x in data if x['ayto:linea'] == bus
                    and x['dc:identifier'][0] == "2"
                    and x['dc:identifier'][5] == periodo
                    and x['dc:identifier'][6] == ident
                    and x['ayto:idEvento'] == "3"
                    and int(x['ayto:hora'])-tiempoSegundos <= limite*3600
                    and int(x['ayto:hora'])-tiempoSegundos >= 0]

    busesAhora = sorted(busesAhora, key=lambda x: x['ayto:hora'])
    
    # Calcular tiempo restante
    tiempoRestante = [math.floor( (int(x['ayto:hora'])-tiempoSegundos) / 60 ) for x in busesAhora]
    
    # Visualizar buses
    #for x in busesAhora:
    #    print('ID: ' + x['dc:identifier'] + '   Hora: ' + str(datetime.timedelta(
    #    seconds=int(x['ayto:hora']))) + '   Parada: ' + str(x['ayto:nombreParada']) + '   Bus: ' + str(x['ayto:linea']) )
        
    return(tiempoRestante)

# Coger JSON de la API
url = "http://datos.santander.es/api/rest/datasets/programacionTUS_horariosLineas.json?items=25923"
data = getResponse(url)['resources']

print(filtrar(data, bus='72', periodo='I', limite=1))
